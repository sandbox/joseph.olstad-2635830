<?php

/**
 * Displays the boost_blast administration page.
 */
function boost_blast_config_system_blast($form, &$form_state) {
  $theme_options = array();
  $theme_options[NULL] = t('Default administration theme');

  foreach (list_themes() as $key => $theme) {
    if ($theme->status) {
      $theme_options[$key] = $theme->info['name'];
    }
  }

  $form['blast']['boost_blast_cache_flush'] = array(
    '#type' => 'checkbox',
    '#title' => t('Blast everything on "cache flushes" aka "cache clears".  (In addition to expire blasting)'),
    '#default_value' => variable_get('boost_blast_cache_flush', FALSE),
    '#description' => t('If checked, boost cache will be blasted on cache clears.'),
  );

  return system_settings_form($form);
}
