<?php

/**
 * @file Drush integration for the boost_blast module.
 */

/**
 * Implements hook_drush_command().
 *
 * @See drush_parse_command() for a list of recognized keys.
 *
 * @return
 *   An associative array describing your command(s).
 */
function boost_blast_drush_command() {
  $items = array();

  $items['boost_blast'] = array(
    'description' => "Immediately clear the boost_blast caches.",
    'drupal dependencies' => array('boost_blast'),
    'aliases' => array('blast','boost-blast','blast-caches'),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function boost_blast_drush_help($section) {
  switch ($section) {
    case 'drush:boost_blast':
      return dt("clear the boost_blast file cache.");
      break;
    case 'drush:blast':
      return dt("clear the boost_blast file cache.");
      break;
    case 'drush:boost-blast':
      return dt("clear the boost_blast file cache.");
      break;
    case 'drush:blast-caches':
      return dt("clear the boost_blast file cache.");
      break;
  }
}

/**
 * Clear the boost_blast cache
 */
function drush_boost_blast() {
  boost_blast_expire_cache(null);
  return TRUE;
}


/**
 * Implements hook_flush_caches()
 */
function drush_boost_blast_flush_caches() {
  if (variable_get('boost_blast_cache_flush')) {
    boost_blast_expire_cache(null);
    drush_log(dt('boost_blast has blasted your boost file caches, if there is anything still cached it might be in your web server cache.'), 'success');
  } else {
    drush_log(dt('boost caches were NOT cleared, to clear boost caches use "drush blast" OR edit your boost_blast settings at admin/config/system/boost/blast.'), 'success');
  }
}
